<?php

use Illuminate\Http\Request;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


/////////////////////////////////////////////////
//              Resourceful Endpoints           //
/////////////////////////////////////////////////
Route::resource('user', 'UserController');
Route::resource('order', 'OrderController');


/////////////////////////////////////////////////
//           unResourceful Endpoints            //
/////////////////////////////////////////////////
Route::post('access_level', 'UserController@accessLevel');


// User specific orders
Route::get('user_orders/{id}', function (Request $request, $id) {
    $data = App\Order::where('user_id', $id)->latest()->get();
    return response(compact('data'), 200);
});

// User cancelled Orders
Route::get('user_cancelled_orders/{id}', function ($id) {
    $data = App\Order::where(['user_id' => $id, 'status' => 'Cancelled'])->latest()->get();
    return response(compact('data'), 200);
});


// Current Orders
Route::get('current_orders', function(Request $request) {
    $data = App\Order::whereDate('created_at', Carbon\Carbon::today())->where('status', 'Available')->latest()->get();
    return response(compact('data'), 200);
});
